Ruby on Rails on Nix example, mostly from these: [blog](https://astronautradio.com/post/149759136113/setting-up-a-nix-environment-for-rails) [code](https://gist.github.com/alexkehayias/f5538193a40d04c48f872bdad505b740) [init script](https://github.com/areina/nix-new-rails-app).

## develop

```
nix-shell --run 'rails server'
```

### build

```
nix-build
```

### update ruby dependencies

Edit Gemfile, then:

```
nix-shell --run 'bundler lock && bundix'
```

## explore

[`default.nix`](defaul.nix) contains the package definition and rails env


### next steps

  - include static files
  - NixOS module with web server and application service ([example](https://gitlab.com/brainrape/skel-django-elm/blob/master/infrastructure/app.nix))
  - cloud infrastructure ([example](https://gitlab.com/brainrape/skel-django-elm/blob/master/infrastructure/main.tf))
