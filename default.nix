{ pkgs ? import ./nixpkgs-pinned.nix {}, ... }:
let
  noc-rubyenv = pkgs.bundlerEnv {
    name = "noc-rubyenv";

    # Setup for ruby gems using bundix generated gemset.nix
    inherit (pkgs) ruby;
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;

    # Bundler groups available in this environment
    groups = ["default" "development" "test"];
  };
in pkgs. stdenv.mkDerivation {
  name = "noc";
  version = "0.0.1";
  src = ./.;

  buildInputs = with pkgs; [
    stdenv
    git

    # Ruby deps
    ruby
    bundler
    bundix

    # Rails deps
    clang
    libxml2
    libxslt
    readline
    sqlite
    openssl
    v8

    noc-rubyenv
    makeWrapper
  ];

  installPhase = ''
    mkdir -p $out/bin
    makeWrapper ${noc-rubyenv}/bin/rails $out/bin/noc-rails --suffix PATH : ${pkgs.v8}/bin
    makeWrapper ${noc-rubyenv}/bin/rake $out/bin/noc-rake
  '';

  shellHook = ''
    export LIBXML2_DIR=${pkgs.libxml2}
    export LIBXSLT_DIR=${pkgs.libxslt}
  '';
}
